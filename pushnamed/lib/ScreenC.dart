import 'package:flutter/material.dart';

class ScreenC extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('ScreenC page'),
      ),
      body: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            ElevatedButton(
              child: const Text('Go to the ScreenA page'),
              onPressed: () {
                Navigator.pushNamed(context, '/');
              },
            ),
            ElevatedButton(
              child: const Text('Go to the ScreenB page'),
              onPressed: () {
                Navigator.pushNamed(context, '/b');
              },
            ),
          ]
      ),
    );
  }
}