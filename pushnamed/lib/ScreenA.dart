import 'package:flutter/material.dart';


class ScreenA extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('ScreenA page'),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
        ElevatedButton(
          child: const Text('Go to the ScreenB page'),
          onPressed: () {
            Navigator.pushNamed(context, '/b');
          },
        ),
          ElevatedButton(
            child: const Text('Go to the ScreenC page'),
            onPressed: () {
              Navigator.pushNamed(context, '/c');
            },
          ),
        ]
      ),
    );
  }
}