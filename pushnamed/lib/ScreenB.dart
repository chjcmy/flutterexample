import 'package:flutter/material.dart';

class ScreenB extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('ScreenB page'),
      ),
      body: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            ElevatedButton(
              child: const Text('Go to the ScreenA page'),
              onPressed: () {
                Navigator.pushNamed(context, '/');
              },
            ),
            ElevatedButton(
              child: const Text('Go to the ScreenC page'),
              onPressed: () {
                Navigator.pushNamed(context, '/c');
              },
            ),
          ]
      ),
    );
  }
}